import random
import time
from gtts import gTTS
import os
import re

import speech_recognition as sr
from fuzzywuzzy import fuzz


def recognize_speech_from_mic(recognizer, microphone):
    # check that recognizer and microphone arguments are appropriate type
    if not isinstance(recognizer, sr.Recognizer):
        raise TypeError("`recognizer` must be `Recognizer` instance")

    if not isinstance(microphone, sr.Microphone):
        raise TypeError("`microphone` must be `Microphone` instance")

    # from the microphone
    with microphone as source:
        recognizer.adjust_for_ambient_noise(source)
        audio = recognizer.listen(source)

    # set up the response object
    response = {
        "success": True,
        "error": None,
        "transcription": None
    }

    try:
        response["transcription"] = recognizer.recognize_google(audio)
    except sr.RequestError:
        # API was unreachable or unresponsive
        response["success"] = False
        response["error"] = "API unavailable"
    except sr.UnknownValueError:
        # speech unintelligible
        response["error"] = "Unable to recognize speech"

    return response


def Boot():
    #print(instructions)
    print("Hi! What would you like to do?")


if __name__ == "__main__":
    # set the list of words, maxnumber of guesses, and prompt limit
    WORDS = ["find", "tell something about you",]

    # create recognizer and mic instances
    recognizer = sr.Recognizer()
    microphone = sr.Microphone()

    # format the instructions string
    instructions = (
        "Those are your options:\n"
        "{words}\n"
    ).format(words=', '.join(WORDS))
    Boot()
    # show instructions and wait 3 seconds before starting 
    time.sleep(1)

    guess = recognize_speech_from_mic(recognizer, microphone)

    print("I didn't catch that. What did you say?\n")

    # if there was an error, stop
    if guess["error"]:
        print("ERROR: {}".format(guess["error"]))

    # show the user the transcription
    print("You said: {}".format(guess["transcription"]))

    # determine if guess is correct and if any attempts remain
    if guess["transcription"].lower() in WORDS:
        print("XDDDDD")
    print(guess["transcription"].lower())

    
    #print("Sorry, you lose!\nI was thinking of '{}'.".format(word))
